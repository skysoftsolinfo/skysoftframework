#!/usr/bin/python
import sys
import os
import platform

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

def screen_shot(name):
    if platform.system() == "Windows":
        name = name.split("\\")[1].split(".py")[0]
        screenshot_folder = "screenshots\\" + name
    else:
        name = name.split("/")[1].split(".py")[0]
        screenshot_folder = "screenshots/" + name
    if not os.path.isdir(screenshot_folder):
        os.mkdir(screenshot_folder)

    return screenshot_folder

if __name__ == "__main__":
    screen_shot(__file__)