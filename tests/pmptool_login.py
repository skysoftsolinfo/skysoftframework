#!/usr/bin/python
"""Login to an admin application"""
import unittest
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from utilities import utils
from conf.configuration import test_bed_name as tb
from conf.configuration import driver_name as driver
from conf.configuration import weburl
from selenium.common.exceptions import NoSuchElementException

class LoginApplication(unittest.TestCase):
    """Login to an application"""

    def setUp(self):
        self.browser = driver
        self.browser.get(weburl)
        self.browser.maximize_window()
        self.screen_shot = utils.screen_shot(__file__) + "/"

    def test_login(self):
        """login method to an application"""
        try:
            username = self.browser.find_element_by_id(tb.get('username'))
            username.click()
            username.send_keys(tb.get('username_value'))
        except NoSuchElementException as e:
            self.browser.save_screenshot(self.screen_shot + "username")
            print "There is no username field or it has changed"

        password = self.browser.find_element_by_id(tb.get('password'))
        password.click()
        password.send_keys(tb.get('pass_value'))
        submit = self.browser.find_element_by_xpath(tb.get('submit'))
        submit.click()
        link = self.browser.find_element_by_xpath(tb.get('link'))
        link.click()


def suite():
    execute = unittest.TestSuite()
    execute.addTest(LoginApplication('test_login'))
    return execute

if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())