#!/usr/bin/python
"""Configurations to execute test cases"""
import argparse
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from test_beds import test_beds

from selenium import webdriver

DRIVER_PATH = "drivers/"

ap = argparse.ArgumentParser()
ap.add_argument("-b", "--bed", required=True, help="Test Bed")
ap.add_argument("-u", "--url", required=True, help="Web URL")

args = vars(ap.parse_args())

test_bed_name, driver_name = None, None

test_bed = args.get("bed")
weburl = args.get("url")

if test_bed == "Chrome":
    test_bed_name = test_beds.chrome
    driver_name = webdriver.Chrome(DRIVER_PATH + 'chromedriver')
elif test_bed == "Firefox":
    test_bed_name = test_beds.firefox
    driver_name = webdriver.Firefox(executable_path=DRIVER_PATH + 'geckodriver')